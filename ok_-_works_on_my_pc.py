import asyncio
import websockets
import json
import os
import hmac
import hashlib
import threading
import asyncio

secret_key = 'supersecretcote_ws'

def comparehash(encoded_message,toencode):
    decoded_message = hmac.new(secret_key.encode(), toencode.encode(), hashlib.sha256).hexdigest()
    return decoded_message==encoded_message


clients = {'check':'ok'}
owners = {'check':'ok'}
pages={'check':'ok'}
disconnec={'check':0}
chk={}
chkcnt=0
def disconnect_check(key):
    if key in owners.keys():
        del disconnec[key]
        return
    if key in disconnec.keys():
        if disconnec[key]>=3:
            if key in clients.keys():
                for client in clients[key]:
                    asyncio.run(client.send(json.dumps({'disconnected':'true'})))
            del disconnec[key]
            return
        else:
            disconnec[key]+=1
    else:
        disconnec[key]=1
    threading.Timer(3, disconnect_check, args=[key]).start()



async def my_handler(websocket, path):
    global chkcnt
    try:
        while True:
            message = await websocket.recv()
            message=json.loads(message)
            if 'jwt' in message.keys():
                if comparehash(message['jwt'],message['cccc']):
                    if message['cccc'] in disconnec.keys():
                        del disconnec[message['cccc']]
                    owners[message['cccc']] = websocket
                    if message['cccc'] in clients.keys():                 
                        total=str(len(clients[message['cccc']]))
                        await websocket.send(json.dumps({'total':total}))
                        for client in clients[message['cccc']]:
                            if not 'page' in message.keys(): 
                                await client.send(json.dumps({'total':total,'page':'0'}))
                                pages[message['cccc']]='0'
                            else:
                                await client.send(json.dumps({'total':total,'page':str(message['page'])}))
                                pages[message['cccc']]=message['page']
                    else:
                        if not 'page' in message.keys(): pages[message['cccc']]='0'
                        else: pages[message['cccc']]=message['page']
                else:
                    await websocket.close()
                    continue
            elif 'page' in message.keys():
                if websocket==owners[message['cccc']]:
                    page=message['page']
                    pages[message['cccc']]=message['page']
                    if message['cccc'] in clients.keys():
                        for client in clients[message['cccc']]:
                            await client.send(json.dumps({'page':str(page)}))
            elif 'chk' in message.keys():
                if message['chk']==1:
                    num=1
                    data={'add':message['user']}
                else: 
                    num=-1
                    data={'remove':message['user']}
                if not message['user'] in chk.keys():
                    chk[message['user']]=[websocket,num]
                    tt=chk[message['user']]
                else:
                    if num==chk[message['user']][1]: return
                    chk[message['user']]=[websocket,num]
                chkcnt+=num
                for ck in chk:
                    await chk[ck][0].send(json.dumps(data))               
            elif 'user' in message.keys():
                if message['user'] in chk.keys():
                    tt=chk[message['user']]
                    if tt[1]==1: await websocket.send(json.dumps({'already':1}))
                    chk[message['user']]=[websocket,tt[1]]                
                else:
                    chk[message['user']]=[websocket,0]
                if chkcnt==0:
                    await websocket.send(json.dumps({'count':'na'}))
                else:
                    await websocket.send(json.dumps({'count':chkcnt}))
            else:
                if message['cccc'] in clients.keys():
                    clients[message['cccc']].append(websocket)
                else:
                    clients[message['cccc']]=[websocket]

                if message['cccc'] in pages.keys():
                    await websocket.send(json.dumps({'page':str(pages[message['cccc']])}))
                total=str(len(clients[message['cccc']]))
                if message['cccc'] in owners.keys():
                    await owners[message['cccc']].send(json.dumps({'total':total}))
                else:
                    await websocket.send(json.dumps({'error':'NA'}))
                    continue
                for client in clients[message['cccc']]:
                    await client.send(json.dumps({'total':total}))
    except Exception as e:
        print(str(e))
        try:
            for key, value in owners.items():
                if value==websocket:
                    del owners[key]
                    try:
                        del pages[key]
                    except: pass
                    threading.Timer(3, disconnect_check, args=[key]).start()
                    # if key in clients.keys():
                        # for client in clients[key]:
                            # await client.send(json.dumps({'disconnected':'true'})) 
                    break

            for key, value in clients.items():
                for index,val in enumerate(value):
                    if val == websocket:
                        clients[key].remove(websocket)
                        total=str(len(clients[key]))
                        if key in owners.keys():
                            await owners[key].send(json.dumps({'total':total}))
                            for client in clients[key]:
                                await client.send(json.dumps({'total':total}))
                        break   
        except: pass
        
port = int(os.environ.get("PORT", 8005))
start_server = websockets.serve(my_handler, "0.0.0.0", port)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
