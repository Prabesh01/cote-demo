<?php
session_start();
      $requestUri = $_SERVER['REQUEST_URI'];
	  $requestUri = strtok($requestUri, '?');
	  $requestUri = ltrim($requestUri, '/');
  if(isset($_SESSION['owns'])){
	  if ($requestUri===''){
		header("Location: http://localhost/".$_SESSION['owns']);
		exit;	  
	  }
	  if($requestUri===$_SESSION['owns']){
		  $owns=1;
	  }else{
		  $owns=0;
	  }
  }
  else{
	  $characters = '23456789abcdefghijkmnpqrstuvwxyz';
	  $randomString = $characters[mt_rand(0, strlen($characters) - 1)] . $characters[mt_rand(0, strlen($characters) - 1)];

      $_SESSION['owns'] = $randomString;
	  if ($requestUri===''){
		  header("Location: http://localhost/".$_SESSION['owns']);
	  }else{
	  $owns=0;
	  }
  }
  
if($owns==1){
function encode_string($message, $secret_key) {
    $encoded_message = hash_hmac('sha256', $message, $secret_key);
    return $encoded_message;
}

$message = $requestUri;
$secret_key = "supersecretcote_ws";

$jwt = encode_string($message, $secret_key);

}
?>  

<title>Demo</title>

<style>

.slideTitle{
    display:none;
}

body {
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  position: relative;
}

.slide {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  display: none;

}

.slide.active {
  display: block;
}
        .slides-canvas {
            width: 100%;
            height: 100%;
        }
#slide-control {
  position: fixed;
  bottom: 20px;
  left: 50%;
  transform: translateX(-50%);
  background-color: rgba(0, 0, 0, 0.8);
  color: #fff;
  padding: 10px;
  border-radius: 5px;
  transition: opacity 0.3s, visibility 0.3s;
  opacity: 0;
  display: hidden;
  <?php if($owns==0){
echo 'display: none;';
  }?>
}

#slidecnt{
  position: fixed;
  bottom: 20px;
  left: 50%;
  transform: translateX(-50%);
  background-color: rgba(0, 0, 0, 0.8);
  color: #fff;
  padding: 10px;
  border-radius: 5px;
  transition: opacity 0.3s, visibility 0.3s;
  opacity: 0;
    <?php if($owns==1){
echo 'display: none;';
  }?>
}

#slidecnt.active {
  opacity: 1;
  visibility: visible;
}

#slide-control.active {
  opacity: 1;
  visibility: visible;
}

.buttons {
  display: flex;
  justify-content: space-between;
  align-items: center;
}

button {
  background: none;
  border: none;
  color: #fff;
  cursor: pointer;
}

#slide-number-btn {
  background-color: transparent;
  font-weight: bold;
}

#slide-list {
  list-style: none;
  padding: 0;
  margin: 0;
  display: none;
}

#slide-list.active {
  display: block;
}

#slide-list li {
  cursor: pointer;
}

#slide-list li:hover {
  background-color: rgba(255, 255, 255, 0.1);
}








#assistive-button {
  position: fixed;
  bottom: 20px;
  right: 20px;
  z-index: 999;
  opacity: 0;
}

#assistive-button.active {
  opacity: 1;
}

.circle-btn {
  position: relative;
  width: 60px;
  height: 60px;
  border-radius: 50%;
  background-color: #ff5722;
  border: none;
  outline: none;
  cursor: pointer;
  transition: background-color 0.3s;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
}

.circle-btn:hover {
  background-color: #e64a19;
}

.digit {
  font-size: 20px;
  font-weight: bold;
  color: #fff;
}

#user-info {
  position: absolute;
  top: 0px;
  left: -100px;
  width: 200px;
  background-color: #fff;
  color: #333;
  padding: 10px;
  border-radius: 5px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2);
  opacity: 0;
  visibility: hidden;
  transition: opacity 0.3s, visibility 0.3s;
}

#assistive-button:hover #user-info {
  opacity: 1;
  visibility: visible;
}

.username {
  font-weight: bold;
}

.fullname {
  font-size: 12px;
  color: #777;
  margin-top: 5px;
}

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css"/>

<?php
                 require 'demo.html';
?>
<div id="slide-control" class="inactive">
  <div class="buttons">
    <button id="prev-btn"><i class="fas fa-chevron-left"></i></button>
    <button id="slide-number-btn">1</button>
    <button id="next-btn"><i class="fas fa-chevron-right"></i></button>
  </div>

  <ul id="slide-list">
    <!-- Add more slides as needed -->
  </ul>
</div>
  <div id="slidecnt">
   <div class="buttons">
      <button id="slide-number-btn slidecntnum">x / x</button>
	   </div>
</div>
<div id="assistive-button">
  <button class="circle-btn">
    <span id="digit" class="digit">0</span>
  </button>
  <div id="user-info">
    <span class="username">@username</span><br>
    <span class="fullname">Presentation Name</span>
  </div>
</div>

<script>
const slides = document.querySelectorAll('.slide');

const ul =document.getElementById('slide-list');

slides.forEach((slide, index) => {
  const li = document.createElement('li');
  li.textContent = `Slide ${index + 1}`;
  li.addEventListener('click', () => {
    changepage(index + 1);
  });
  ul.appendChild(li);
});

const slidecntnum = document.getElementById('slide-number-btn slidecntnum');
const digit = document.getElementById('digit');

var ws = new WebSocket("ws://127.0.0.1:8005");
slidecntnum.innerHTML='1 / '+slides.length;
function parse(event) {
  const data = JSON.parse(event.data);
  <?php if($owns==0){?>
	if(data.error){ // no  owner. wait. redirect
	  alert('No Host is online. Redirecting you..')
	  window.location.href = "/<?php echo $requestUri.'/view/' ?>";
  }if(data.page){ // page.
	  var activeSlide = document.querySelector('.slide.active');
	  var activeSlideIndex = Array.prototype.indexOf.call(slides, activeSlide);
		if(data.page!=activeSlideIndex){
	  activeSlide.classList.remove('active');
	  slides[data.page].classList.add('active');
	  slidecntnum.innerHTML=parseInt(data.page)+1+' / '+slides.length;
		}
	  
   } //  if(data.disconnected){ // owner disconnected.
	  // alert('Host disconnected. Redirecting you..')
	  // window.location.href = "/<?php echo $requestUri.'/view/' ?>";
  // }
  <?php } ?>
  if(data.total){ // total users
	  // alert(data.total)
	  digit.innerHTML=data.total;
	   }
}
	
function connectWebSocket() {
  ws = new WebSocket('ws://127.0.0.1:8005');

  ws.onopen = function (event) {
try{
  var activeSlide = document.querySelector('.slide.active');
  var activeSlideIndex = Array.prototype.indexOf.call(slides, activeSlide);
}catch{
	activeSlideIndex=0
}
    ws.send(JSON.stringify(<?php if($owns==1){ echo '{"cccc": "'.$requestUri.'","jwt":"'.$jwt.'","page": activeSlideIndex'; }else{ echo '{"cccc": "'.$requestUri.'"';} ?>}));
  };

  ws.onmessage = parse;

  ws.onclose = function (event) {
    setTimeout(connectWebSocket, 1000);
  };
}

connectWebSocket();


document.addEventListener('keydown', function(event) {
	<?php if($owns==1){?>
  var activeSlide = document.querySelector('.slide.active');
  var activeSlideIndex = Array.prototype.indexOf.call(slides, activeSlide);
  if (event.key === 'ArrowLeft' || event.key === 'PageUp') {
    activeSlide.classList.remove('active');

    var previousSlideIndex = (activeSlideIndex - 1 + slides.length) % slides.length;
    slides[previousSlideIndex].classList.add('active');
		slideNumberBtn.innerHTML=previousSlideIndex+1;

	ws.send({"page":previousSlideIndex,"cccc":"<?php echo $requestUri ?>"});

  }

  if (event.key === 'ArrowRight' ||  event.key === 'PageDown') {
    activeSlide.classList.remove('active');

    var nextSlideIndex = (activeSlideIndex + 1) % slides.length;
    slides[nextSlideIndex].classList.add('active');
	slideNumberBtn.innerHTML=nextSlideIndex+1;

	ws.send({"page":nextSlideIndex,"cccc":"<?php echo $requestUri ?>"});

  }
  
	<?php } ?>
    if (event.key === 'F5') {
openFullscreen();
  }
   if (event.key === 'Escape') {
exitFullscreen();
  }
});



document.querySelectorAll('.slide')[0].classList.add('active');
	<?php if($owns==1){?>
  document.addEventListener('touchstart', function(event) {
  if (event.touches.length === 1) {
    const touchDuration = Date.now() - event.touches[0].timestamp;
    const longTouchThreshold = 500;
    // if (touchDuration < longTouchThreshold) {
  var activeSlide = document.querySelector('.slide.active');
  var activeSlideIndex = Array.prototype.indexOf.call(slides, activeSlide);

    activeSlide.classList.remove('active');

    var nextSlideIndex = (activeSlideIndex + 1) % slides.length;
    slides[nextSlideIndex].classList.add('active');
slideNumberBtn.innerHTML=nextSlideIndex+1;

	ws.send({"page":nextSlideIndex,"cccc":"<?php echo $requestUri ?>"});
    // }
  }
});
	<?php } ?>
function openFullscreen() {
event.preventDefault()
var elem = document.getElementsByClassName("active")[0];

  if (document.body.requestFullscreen) {
    document.body.requestFullscreen();
  } else if (document.body.webkitRequestFullscreen) {
    document.body.webkitRequestFullscreen();
  } else if (document.body.msRequestFullscreen) {
    document.body.msRequestFullscreen();
  }
}

function exitFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    document.msExitFullscreen();
  }
}


</script>

<script>
const slideControl = document.getElementById('slide-control');
const slidecnt = document.getElementById('slidecnt');
const slideNumberBtn = document.getElementById('slide-number-btn');
const slideList = document.getElementById('slide-list');
const slideLeft = document.getElementById('prev-btn');
const slideRight = document.getElementById('next-btn');

const assistiveButton = document.getElementById('assistive-button');
const userInfo = document.getElementById('user-info');

assistiveButton.addEventListener('mousemove', () => {
  userInfo.style.opacity = '1';
  userInfo.style.visibility = 'visible';
});

assistiveButton.addEventListener('mouseleave', () => {
  userInfo.style.opacity = '0';
  userInfo.style.visibility = 'hidden';
});


let isActive = true;
let timeout;

function hideSlideControl() {
  isActive = false;
  slideControl.classList.remove('active');
  assistiveButton.classList.remove('active');
  slidecnt.classList.remove('active');
}

function showSlideControl() {
  isActive = true;
  slideControl.classList.add('active');
  assistiveButton.classList.add('active');
  slidecnt.classList.add('active');
}

function resetTimeout() {
  clearTimeout(timeout);
  timeout = setTimeout(hideSlideControl, 3000);
}

slideControl.addEventListener('mousemove', () => {
    showSlideControl();
    resetTimeout();
});

assistiveButton.addEventListener('mousemove', () => {
    showSlideControl();
    resetTimeout();
});

assistiveButton.addEventListener('click', () => {
	if (document.fullscreenElement === document.body) {
exitFullscreen();
	}else{
		openFullscreen();
	}
});

slidecnt.addEventListener('mousemove', () => {
    showSlideControl();
    resetTimeout();
});

document.addEventListener('keypress', (event) => {
  if (event.code === 'Space') {
    showSlideControl();
    resetTimeout();
  }
});

slideNumberBtn.addEventListener('click', () => {
  slideList.classList.toggle('active');
});

slideLeft.addEventListener('click', () => {
  var activeSlide = document.querySelector('.slide.active');
  var activeSlideIndex = Array.prototype.indexOf.call(slides, activeSlide);
    activeSlide.classList.remove('active');

    var previousSlideIndex = (activeSlideIndex - 1 + slides.length) % slides.length;
    slides[previousSlideIndex].classList.add('active');
		slideNumberBtn.innerHTML=previousSlideIndex+1;

	ws.send({"page":previousSlideIndex,"cccc":"<?php echo $requestUri ?>"});

});

slideRight.addEventListener('click', () => {
	  var activeSlide = document.querySelector('.slide.active');
  var activeSlideIndex = Array.prototype.indexOf.call(slides, activeSlide);

    activeSlide.classList.remove('active');

    var nextSlideIndex = (activeSlideIndex + 1) % slides.length;
    slides[nextSlideIndex].classList.add('active');
	slideNumberBtn.innerHTML=nextSlideIndex+1;

	ws.send({"page":nextSlideIndex,"cccc":"<?php echo $requestUri ?>"});

});

function changepage(n){
		  var activeSlide = document.querySelector('.slide.active');
    activeSlide.classList.remove('active');
slides[n-1].classList.add('active');
	slideNumberBtn.innerHTML=n;

	ws.send({"page":n,"cccc":"<?php echo $requestUri ?>"});

}

slideList.addEventListener('mouseleave', () => {
  slideList.classList.remove('active');
});

showSlideControl();
resetTimeout();
</script>