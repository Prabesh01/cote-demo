<?php
session_start();
  if(!(isset($_SESSION['user']))){
	  $characters = '23456789abcdefghijkmnpqrstuvwxyz';
$randomString = '';
$length = mt_rand(4, 8);

for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[mt_rand(0, strlen($characters) - 1)];
}


      $_SESSION['user'] = $randomString;

  }
 $user= $_SESSION['user'];
?>
<!--
Used CSS From:
 - https://codepen.io/remersonc/full/KapeLy
 - https://codepen.io/vsync/pen/jOWOXWG
 
I am a backend guy; i suck at frontend
-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>c/Demo</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee|Roboto+Condensed:400,400i,700" rel="stylesheet">
<style>

* {
  box-sizing: border-box;
  font-family: inherit;
}
body {
  margin: 0;
  font-family: 'Roboto Condensed';
  line-height: 1.3;
  color: #000080;
  background-color: rgba(0,0,128,0.05);
}
h1,
h2,
h3,
h4 {
  font-family: 'Bungee';
  font-weight: normal;
}
h1,
h2,
h3,
h4,
p,
ul {
  margin: 0;
  padding: 0;
}
h2 {
  font-size: 2rem;
}
h3 {
  color: rgba(0,0,128,0.5);
}
blockquote {
  margin: 1rem 2rem;
  padding: 0 0 0 2rem;
}
@media screen and (max-width: 32rem) {
  blockquote {
    margin: 1rem;
  }
}
svg {
  display: block;
}
a {
  display: inline-block;
  color: #ff0080;
  transition: 0.2s ease;
}
button {
  outline: none;
  padding: 0.5rem;
  cursor: pointer;
  border: 2px solid transparent;
  border-radius: 0.125rem;
  color: #fff;
  background-color: #0080ff;
  font-size: inherit;
  transition: 0.2s ease;
}
button:focus,
button:hover {
  background-color: #40a0ff;
}
button:disabled {
  pointer-events: none;
  color: rgba(0,0,128,0.25);
  border-color: rgba(0,0,128,0.05);
  background-color: rgba(0,0,128,0.1);
}
main {
  display: flex;
  flex-flow: column nowrap;
  margin: 0 auto;
  padding: 2rem;
  max-width: 32rem;
  width: 100%;
  min-height: 100vh;
  border: 1px solid #eee;
  background-color: #fff;
}
@media screen and (max-width: 32rem) {
  main {
    border: none;
    padding: 1rem;
  }
}
.page {
  display: flex;
  flex-flow: column;
  flex: 1;
}
.page > *:nth-child(1) {
  animation: fadeIn 0.5s 0.1s ease both;
}
.page > *:nth-child(2) {
  animation: fadeIn 0.5s 0.2s ease both;
}
.page > *:nth-child(3) {
  animation: fadeIn 0.5s 0.3s ease both;
}
.page > *:nth-child(4) {
  animation: fadeIn 0.5s 0.4s ease both;
}
.page > *:nth-child(5) {
  animation: fadeIn 0.5s 0.5s ease both;
}
.page > *:nth-child(6) {
  animation: fadeIn 0.5s 0.6s ease both;
}
.credits {
  margin: auto -2rem 0 -2rem;
  padding: 1rem 2rem 0 2rem;
  border-top: 1px solid rgba(0,0,128,0.25);
}
@media screen and (max-width: 32rem) {
  .credits {
    margin: auto -1rem 0 -1rem;
  }
}
.progress {
  display: flex;
  align-items: center;
  margin: 0.25rem -0.25rem 0rem -0.25rem;
}
.progress * {
  margin: 0 0.25rem;
}
.progress span {
  position: relative;
  flex: 1;
  padding: 0.125rem;
  border-radius: 0.125rem;
  background-color: rgba(0,0,128,0.1);
  overflow: hidden;
}
.progress span.done:before {
  transform: scaleX(1);
}
.progress span:before {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #ff0080;
  transform-origin: 0% 50%;
  transform: scaleX(0);
  transition: transform 0.25s ease;
}
.progress small {
  margin-left: 0.5rem;
}
.quote-container {
  position: relative;
  margin: 1rem -2rem 0 -2rem;
  counter-reset: quotes;
  overflow: hidden;
}
@media screen and (max-width: 32rem) {
  .quote-container {
    margin: 1rem -1rem 0 -1rem;
  }
}
.list {
  position: relative;
  z-index: 2;
  display: flex;
  transition: 0.5s ease;
}
.item {
  flex-shrink: 0;
  width: 100%;
  transition: transform 0.5s ease, opacity 0.5s ease;
}
.item.show {
  opacity: 1;
}
.rating {
  font-size: 3.5rem;
}
.stars {
  position: relative;
  margin: 0 auto;
  width: fit-content;
}
.stars div {
  color: #ff0080;
  text-align: center;
}
.stars div:last-child {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  clip-path: polygon(0% 0%, 0% 0%, 0% 100%, 0% 100%);
  transition: 0.5s 0.5s ease;
}
.badge {
  position: absolute;
  top: 0.5rem;
  left: 2rem;
  font-family: 'Bungee';
  font-size: 1.5rem;
  color: #ff0080;
}
.badges {
	  border-top: 0.125rem solid rgba(0,0,128,0.25);
  border-bottom: 0.125rem solid rgba(0,0,128,0.25);
    position: relative;
  margin: 1rem 0 2rem 0;
  overflow: hidden;
  contain: layout paint;

}
.icon {
  position: relative;
  z-index: 4;
  margin: 0 auto;
  padding: 2rem;
  width: 10rem;
  animation: float 5s ease both infinite;
}
.icon svg {
  transform: translateX(5rem);
  transition: transform 1s ease;
}
.item.show .icon svg {
  transform: translateX(0);
}
.bar {
  position: absolute;
  height: 0.25rem;
  border-radius: 0.5rem;
  animation: glide 10s ease both infinite;
}
.quote {
  position: relative;
  opacity: 0;
  transition: 0.25s ease;
  transition: opacity 0.25s ease;
}
.item.show .quote {
  opacity: 1;
}
.quote:before {
  content: '\201c';
  position: absolute;
  top: -0.5rem;
  left: 0;
  line-height: 1;
  font-family: 'Bungee';
  font-size: 2.5rem;
  color: #ff0080;
}
.author {
  margin-top: 0.5rem;
  text-align: right;
  text-transform: uppercase;
}
.options {
  display: flex;
  margin: 1rem;
  background-color: #fff;
  border-radius: 0.125rem;
  border: 0.125rem solid #ff0080;
  overflow: hidden;
}
.options button {
  flex: 1;
  border-radius: 0;
  border: none;
  color: #ff0080;
  background-color: transparent;
  text-transform: uppercase;
}
.options button:focus,
.options button:hover {
  background-color: rgba(255,0,128,0.05);
}
.options button.selected {
  color: #fff;
  background-color: #ff0080;
}
.options button:nth-child(n+2) {
  border-left: 0.125rem solid;
  border-color: inherit;
}
.options button i {
  display: inline-block;
  font-size: 1.75rem;
}
.options button span {
  display: block;
  font-size: 0.75rem;
}
.controls {
  display: flex;
  justify-content: space-between;
  margin: 2rem -0.5rem 0 -0.5rem;
}
.controls button {
  flex-shrink: 0;
  margin: 0.5rem;
  border-radius: 0;
}
.controls button > div {
  display: flex;
  align-items: center;
}
.controls button.previous {
  margin-right: auto;
  padding-right: 1.25em;
}
.controls button.next {
  margin-left: auto;
  margin-right: auto;
}
.rating-container {
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
  width: 100%;
  text-align: center;
}
.rating-container h4 {
  margin-top: 1rem;
  font-size: 10rem;
  line-height: 1;
}
@media screen and (max-width: 32rem) {
  .rating-container h4 {
    font-size: 6rem;
  }
}
.rating-container p {
  margin-top: 4rem;
}
.star-container {
  display: flex;
  justify-content: center;
  margin-bottom: 1rem;
  color: #ff0080;
}
.star-container > div {
  position: relative;
  display: flex;
}
.star-container i {
  flex-shrink: 0;
  font-size: 3rem;
}
.stars {
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  width: 0;
  overflow: hidden;
  transition: width 0.5s 0.25s ease;
}
@-moz-keyframes slideDown {
  from {
    transform: translateY(-1rem);
  }
  to {
    transform: translateY(0);
  }
}
@-webkit-keyframes slideDown {
  from {
    transform: translateY(-1rem);
  }
  to {
    transform: translateY(0);
  }
}
@-o-keyframes slideDown {
  from {
    transform: translateY(-1rem);
  }
  to {
    transform: translateY(0);
  }
}
@keyframes slideDown {
  from {
    transform: translateY(-1rem);
  }
  to {
    transform: translateY(0);
  }
}
@-moz-keyframes fadeIn {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}
@-webkit-keyframes fadeIn {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}
@-o-keyframes fadeIn {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}
@keyframes fadeIn {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}
@-moz-keyframes fadeOut {
  from {
    opacity: 1;
  }
  to {
    opacity: 0;
  }
}
@-webkit-keyframes fadeOut {
  from {
    opacity: 1;
  }
  to {
    opacity: 0;
  }
}
@-o-keyframes fadeOut {
  from {
    opacity: 1;
  }
  to {
    opacity: 0;
  }
}
@keyframes fadeOut {
  from {
    opacity: 1;
  }
  to {
    opacity: 0;
  }
}
@-moz-keyframes slideRight {
  from {
    transform: translateX(-1rem);
  }
  to {
    transform: translateX(0);
  }
}
@-webkit-keyframes slideRight {
  from {
    transform: translateX(-1rem);
  }
  to {
    transform: translateX(0);
  }
}
@-o-keyframes slideRight {
  from {
    transform: translateX(-1rem);
  }
  to {
    transform: translateX(0);
  }
}
@keyframes slideRight {
  from {
    transform: translateX(-1rem);
  }
  to {
    transform: translateX(0);
  }
}
@-moz-keyframes float {
  50% {
    transform: translateY(-0.5rem);
  }
}
@-webkit-keyframes float {
  50% {
    transform: translateY(-0.5rem);
  }
}
@-o-keyframes float {
  50% {
    transform: translateY(-0.5rem);
  }
}
@keyframes float {
  50% {
    transform: translateY(-0.5rem);
  }
}
@-moz-keyframes glide {
  to {
    transform: translateX(-40rem);
  }
}
@-webkit-keyframes glide {
  to {
    transform: translateX(-40rem);
  }
}
@-o-keyframes glide {
  to {
    transform: translateX(-40rem);
  }
}
@keyframes glide {
  to {
    transform: translateX(-40rem);
  }
}



  .listt {
    margin: auto;
    display: flex;
    flex-direction: column;
	columns: 1;
  }
  .listt > * {
    background: rgba(255, 255, 255, 0.5);
    border-radius: 5px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
    transition: 0.2s cubic-bezier(0, 0.6, 0.25, 1);
  }
  .listt > input {
    display: none;
  }
  .listt > label {
    position: relative;
    z-index: 5;
    display: block;
    background: #745fb5;
    color: white;
    cursor: pointer;
    user-select: none;
    break-before: column;
  }
  .listt > label:hover {
    opacity: 0.8;
  }
</style>
<main id="app">
   <div data-reactroot="" class="page">
        <div class="options">
  <button><a href="https://cote.ws" style="text-decoration:none;"><span>cote.ws</span></a></button>
<button class=""><a href="https://demo.cote.ws" style="text-decoration:none;"><span>demo.cote.ws</span></a></button><button class=""><a href="https://app.cote.ws" style="text-decoration:none;"><span>app.cote.ws</span></a></button><button class=""><a href="https://info.cote.ws" style="text-decoration:none;"><span>info.cote.ws</span></a></button>
<button class=""><span>@<?php echo $_SESSION['user'];?></span></button>
</div>
		&nbsp; &nbsp; &nbsp; &nbsp;

      <h2>Demo Check-In</h2>
      <div class="progress"><span class=""></span><span class=""></span><span class=""></span><span class=""></span></div>
      <div class="quote-container">
         <span class="bar" style="z-index: 0; top: 1.81459rem; right: -3rem; width: 0.5rem; background-color: rgba(0, 0, 128, 0.424); animation-duration: 11.7827s; animation-delay: 0.291651s;"></span><span class="bar" style="z-index: 1; top: 3.34144rem; right: -3rem; width: 1.36755rem; background-color: rgba(0, 0, 128, 0.24); animation-duration: 9.61226s; animation-delay: 0.222203s;"></span><span class="bar" style="z-index: 1; top: 9.94592rem; right: -3rem; width: 0.996981rem; background-color: rgba(0, 0, 128, 0.263); animation-duration: 8.91647s; animation-delay: -0.737999s;"></span><span class="bar" style="z-index: 2; top: 4.10806rem; right: -3rem; width: 2.79325rem; background-color: rgba(0, 0, 128, 0.353); animation-duration: 9.80429s; animation-delay: 0.838162s;"></span><span class="bar" style="z-index: 2; top: 6.63821rem; right: -3rem; width: 2.20516rem; background-color: rgba(0, 0, 128, 0.255); animation-duration: 19.2966s; animation-delay: 0.556425s;"></span><span class="bar" style="z-index: 3; top: 7.54554rem; right: -3rem; width: 0.609949rem; background-color: rgba(0, 0, 128, 0.176); animation-duration: 11.7142s; animation-delay: -0.0344592s;"></span><span class="bar" style="z-index: 3; top: 6.17262rem; right: -3rem; width: 0.5rem; background-color: rgba(0, 0, 128, 0.36); animation-duration: 14.1464s; animation-delay: 0.0852522s;"></span><span class="bar" style="z-index: 4; top: 6.43793rem; right: -3rem; width: 2.92181rem; background-color: rgba(0, 0, 128, 0.204); animation-duration: 11.9522s; animation-delay: 0.809365s;"></span><span class="bar" style="z-index: 4; top: 4.43165rem; right: -3rem; width: 2.3157rem; background-color: rgba(0, 0, 128, 0.255); animation-duration: 8.13437s; animation-delay: 0.797625s;"></span><span class="bar" style="z-index: 5; top: 10.2184rem; right: -3rem; width: 2.08489rem; background-color: rgba(0, 0, 128, 0.39); animation-duration: 4.85133s; animation-delay: 0.143229s;"></span>
         <div class="list" style="transform: translateX(0%);">
            <div class="item show">
			 <div class="badges">
               <div id="badge" class="badge">0</div>
                  <div id="icon" class="icon">
                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120">
                        <defs>
                           <clipPath id="a">
                              <path d="M0 96h96V0H0v96z"></path>
                           </clipPath>
                        </defs>
                        <g clip-path="url(#a)" transform="matrix(1.063 0 0 -1.063 7.942 111.556)" stroke="#ff0080" fill="none" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10">
                           <path fill="white" d="M81.7915 52.1182c5.036-18.662-6.011-37.874-24.673-42.91-18.663-5.035-37.874 6.011-42.91 24.674-5.036 18.662 6.011 37.873 24.673 42.909 18.662 5.036 37.874-6.01 42.91-24.673z"></path>
                           <path d="M78.7798 59.6763c2.874-.233 5.404-.681 7.506-1.338 3.645-1.139 6.005-2.908 6.647-5.285 2.158-7.998-15.976-19.848-40.504-26.466-24.528-6.619-46.161-5.5-48.319 2.498-.605 2.242.385 4.787 2.664 7.434M94 81c0-4.418-3.582-8-8-8s-8 3.582-8 8 3.582 8 8 8 8-3.582 8-8zm-23 5c0-1.657-1.343-3-3-3s-3 1.343-3 3 1.343 3 3 3 3-1.343 3-3zM21.0684 44.9165c.104 1.45.324 2.878.655 4.274m2.7082 6.959c3.424 6.131 9.223 10.929 16.539 12.902"></path>
                        </g>
                     </svg>
                  </div>
				  </div>
               <blockquote class="quote show">
                  <p class="text">Unlike the complete app, no database is used in this demo so the list below doesn't show any past check-ins. In the app, there are options for result export, collaboration, chat, message boradcast, categories, user redirect, access control, private/public results, anonymous check-in and more.</p>
                  <p class="author">
                     <strong>
                        — &nbsp;@creater
                     </strong>
                  </p>
               </blockquote>
            </div>
         </div>
      </div>
	        <div class="controls">
         <button id="chkbtn" class="next" >
            <div>
				<c id="chktxt">Check-In </c><i class="material-icons">keyboard_arrow_right</i>
            </div>
         </button>
      </div>

	  
	  <!-- <div class="options"><button class=""><span>Home</span></button><button class=""><span>Login</span></button></div> -->
	  
		&nbsp; &nbsp; &nbsp; &nbsp;
      <p class="credits">
         <small>
				<!-- Anonymous Check-In. No Information is shared. -->
				Random username is used here just for demo. In the app, email login identification is used.
         </small>
      </p>
	  		&nbsp; &nbsp; &nbsp; &nbsp;

<div class="rating-container"><div><h3>Check-Ins</h3>

<div id="list" class="listt" data-columns="1 2 3 4">
  <input type="checkbox" checked="checked" id="col-1" />
  <label for="col-1">Username</label>
  <div id="srv">item-1</div>
</div>


</div>	  
	  
	  
   </div>
</main>

<script>
const chkbtn=document.getElementById('chkbtn');
var track=0;
chkbtn.addEventListener('click', () => {
	if (track == 0) {
		track=1;
	ws.send(JSON.stringify({"chk":1,"user":"<?php echo $user; ?>"}));
	}else{
		document.getElementById('chktxt').textContent='Check-In';
		track=0;
		document.getElementById('<?php echo $user; ?>').remove();
		cntbd=document.getElementById('badge').innerHTML;
		if(parseInt(cntbd)!=0){
			document.getElementById('badge').innerHTML=parseInt(cntbd)-1;
		}
	ws.send(JSON.stringify({"chk":-1,"user":"<?php echo $user; ?>"}));
	}
});


</script>

<script>
var ws = new WebSocket("ws://127.0.0.1:8005");
function parse(event) {
  const data = JSON.parse(event.data);
	if(data.add){
		// document.getElementById('chktxt').textContent='UNDO';
		document.getElementById('list').innerHTML+='<div id="'+data.add+'">@'+data.add+'2</div>';
		cntbd=document.getElementById('badge').innerHTML;
		document.getElementById('badge').innerHTML=parseInt(cntbd)+1;

  }if(data.remove){ 
		// document.getElementById('chktxt').textContent='Check-In';
		try{
		document.getElementById(data.remove).remove();
		}catch{}
		cntbd=document.getElementById('badge').innerHTML;
		if(parseInt(cntbd)!=0){
			document.getElementById('badge').innerHTML=parseInt(cntbd)-1;
		}
   }if(data.already){ 
   track=1;
		document.getElementById('chktxt').textContent='UNDO';
		document.getElementById('list').innerHTML+='<div id="<?php echo $user; ?>">@<?php echo $user; ?>2</div>';
		cntbd=document.getElementById('badge').innerHTML;
		document.getElementById('badge').innerHTML=parseInt(cntbd)+1;
   }if(data.count){ 
	if(data.count=='na'){
		track=0;
				document.getElementById('badge').innerHTML=0;
		document.getElementById('chktxt').textContent='Check-In';	
const listDiv = document.getElementById("list");
const divElements = listDiv.getElementsByTagName("div");

const divArray = Array.from(divElements);

divArray.forEach((div) => {
  div.remove();
});
		
	}else{
		document.getElementById('badge').innerHTML=data.count;
	}
   }
}
	
function connectWebSocket() {
  ws = new WebSocket('ws://127.0.0.1:8005');

  ws.onopen = function (event) {
    ws.send(JSON.stringify(<?php echo '{"user": "'.$user.'"}'; ?>));
  };

  ws.onmessage = parse;

  ws.onclose = function (event) {
    setTimeout(connectWebSocket, 1000);
  };
}

connectWebSocket();


</script>

    <script type="text/javascript">
icons_list=['<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120"><defs><clipPath id="a"><path d="M0 96h96V0H0v96z"/></clipPath></defs><g clip-path="url(#a)" transform="matrix(1.087 0 0 -1.087 6.74 112.173)" stroke="#ff0080" fill="none" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"><path fill="white" d="M24 88c0-2.208-1.793-4-4-4s-4 1.792-4 4 1.793 4 4 4 4-1.792 4-4zm43-27c0-2.208-1.793-4-4-4s-4 1.792-4 4 1.793 4 4 4 4-1.792 4-4zM39 24c0-2.208-1.793-4-4-4s-4 1.792-4 4 1.793 4 4 4 4-1.792 4-4zM10 76c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zm37-50c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zM31 39c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zm20 19c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zm39-40.9678c.019-6.077-4.891-11.014-10.968-11.032-6.076-.019-11.013 4.891-11.032 10.968-.019 6.073 4.891 11.013 10.968 11.032 6.076.015 11.013-4.894 11.032-10.968z"/><path fill="white" d="M66.998 61.1113c4.418.013 7.894-3.558 7.907-7.977.012-4.418-3.558-8.01-7.977-8.023-4.418-.013-8.011 3.558-8.023 7.977-.006 1.799.584 3.462 1.583 4.802"/><path d="M70.6113 9.8857c-5.948-3.711-12.971-5.863-20.497-5.886-21.537-.064-39.05 17.346-39.114 38.888-.061 21.538 17.348 39.051 38.885 39.112 21.538.064 39.05-17.345 39.115-38.884.017-6.058-1.348-11.797-3.798-16.92"/></g></svg>','<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120"><defs><clipPath id="a"><path d="M0 96h96V0H0v96z"/></clipPath></defs><g clip-path="url(#a)" transform="matrix(1.042 0 0 -1.042 10 110)" stroke="#ff0080" fill="none" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"><path fill="white" d="M67 93c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zm13-52C80 19.461 62.539 2 41 2S2 19.461 2 41s17.461 39 39 39 39-17.461 39-39zm14 42.0322c.019-6.077-4.891-11.014-10.968-11.032-6.077-.019-11.013 4.891-11.032 10.968-.02 6.073 4.891 11.013 10.968 11.032 6.077.015 11.012-4.894 11.032-10.968z"/><path d="M54.3965 77.6372s-9.532-2.575-5.97-10.387c3.563-7.813 7.574-7.038 5.574-14.113v.004c0 .103-.187.202-.386.296 0 0-7.839 5.125-17.277.688-9.437-4.438-8.048-16.446-3.075-17.688 4.008-1 6.321-2.604 7.026-6.187.632-3.218.712-11.031 3.743-14.344 2.464-2.692 9.463-4.724 13.338 4.276 2.718 6.317 5.168 17.818 2.666 25.818h-.002c14.095-2 19.34 2.12 19.34 2.12M14.8975 69.975s8.228-4.801 3.728-13.826c-4.5-9.024-7.487-9.157-8-16.524-.451-6.459 9.724-18.036-.451-22.518"/></g></svg>','<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120"><defs><clipPath id="a"><path d="M0 96h96V0H0v96z"/></clipPath></defs><g clip-path="url(#a)" transform="matrix(1.042 0 0 -1.042 10 110)" stroke="#ff0080"><path d="M10 6c0-2.208-1.793-4-4-4S2 3.792 2 6s1.793 4 4 4 4-1.792 4-4zm84 80c0-4.418-3.582-8-8-8s-8 3.582-8 8 3.582 8 8 8 8-3.582 8-8zm-8-42C86 22.461 68.539 5 47 5S8 22.461 8 44s17.461 39 39 39 39-17.461 39-39zm-1 6H55m31-6H68M11 29h47M9 37h23M15 66h35M10 56h12m10 0h4m41-37H59m-7 0h-2m-8 18h6" fill="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"/></g></svg>','<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120"><defs><clipPath id="a"><path d="M0 96h96V0H0v96z"/></clipPath></defs><g clip-path="url(#a)" transform="matrix(1.042 0 0 -1.042 10 108.96)" stroke="#ff0080" fill="none" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"><path fill="white" d="M10 80c0-2.208-1.793-4-4-4s-4 1.792-4 4 1.793 4 4 4 4-1.792 4-4zm61.8286-58.828c-1.562-1.562-4.096-1.561-5.657 0s-1.561 4.096 0 5.656c1.561 1.562 4.096 1.561 5.657 0 1.561-1.56 1.561-4.095 0-5.656zM27 88c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zm38 2c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zM9 64c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zM30 7c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zm-16 9c0-3.313-2.687-6-6-6s-6 2.687-6 6 2.687 6 6 6 6-2.687 6-6zm64 60c-4.418 0-8 3.582-8 8s3.582 8 8 8 8-3.582 8-8-3.582-8-8-8z"/><path d="M83.046 77.7954C89.877 69.7684 94 59.3654 94 48.0004c0-25.406-20.597-46-46-46M24.223 44.7148c-.147 1.074-.223 2.171-.223 3.285m1.745 9.0022c2.59 6.396 7.858 11.423 14.414 13.688"/><path d="M65.901 21.4717c-5.109-3.455-11.269-5.472-17.901-5.472-17.673 0-32 14.327-32 32 0 17.673 14.327 32 32 32 17.673 0 32-14.327 32-32 0-8.162-3.056-15.611-8.086-21.263"/></g></svg>','<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120"><defs><clipPath id="a"><path d="M0 96h96V0H0v96z"/></clipPath></defs><g clip-path="url(#a)" transform="matrix(1.044 0 0 -1.044 9.897 108.296)" stroke="#ff0080" fill="none" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"><path fill="white" d="M50 42c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zM23 6c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zm-9 74c0-.552-.448-1-1-1s-1 .448-1 1 .448 1 1 1 1-.448 1-1zm26-46c0-4.418-3.582-8-8-8s-8 3.582-8 8 3.582 8 8 8 8-3.582 8-8zm34.4302 53.539c6.077.019 11.014-4.891 11.032-10.968.02-6.076-4.89-11.013-10.968-11.032-6.073-.019-11.012 4.891-11.032 10.968-.015 6.076 4.895 11.013 10.968 11.032z"/><path d="M81.5767 68.1504c3.711-5.948 5.864-12.971 5.886-20.497.065-21.537-17.345-39.05-38.887-39.114-21.539-.061-39.052 17.348-39.113 38.885-.064 21.538 17.345 39.05 38.884 39.115 6.058.017 11.797-1.348 16.92-3.798"/><path d="M24.7422 37.3467c-15.429 8.229-24.748 18.492-22.237 25.957 1.517 4.51 7.093 7.225 15.08 8.056M92.921 39.58c1.049-2.445 1.283-4.776.574-6.884-3.345-9.947-26.426-11.157-51.553-2.705-.866.291-1.724.591-2.572.898"/></g></svg>','<?xml version="1.0"?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 120"><defs><clipPath id="a"><path d="M0 96h96V0H0v96z"/></clipPath></defs><g clip-path="url(#a)" transform="matrix(1.063 0 0 -1.063 9.006 110.493)" stroke="#ff0080" fill="none" stroke-width="4" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10"><path fill="white" d="M80.791 51.1182c5.036-18.662-6.01-37.874-24.673-42.909-18.663-5.036-37.874 6.01-42.909 24.673-5.036 18.662 6.01 37.873 24.673 42.909 18.662 5.036 37.874-6.01 42.909-24.673z"/><path d="M77.7793 58.6763c2.875-.233 5.404-.681 7.507-1.338 3.644-1.139 6.005-2.908 6.646-5.285 2.159-7.999-15.975-19.848-40.504-26.466-24.528-6.619-46.161-5.5-48.319 2.498-.605 2.243.385 4.787 2.664 7.434M93 80c0-4.418-3.582-8-8-8s-8 3.582-8 8 3.582 8 8 8 8-3.582 8-8zm-23 5c0-1.657-1.344-3-3-3s-3 1.343-3 3 1.344 3 3 3 3-1.343 3-3zM20.0684 43.916c.104 1.45.324 2.879.654 4.274m2.7092 6.9594c3.424 6.131 9.223 10.929 16.539 12.902"/><path d="M88.2363 34.006c2.79-3.711 3.872-7.215 2.788-10.115-3.286-8.795-25.242-8.717-49.04.176-23.798 8.892-40.426 23.231-37.14 32.025 1.438 3.849 6.45 5.998 13.625 6.454"/></g></svg>']

const length = icons_list.length;
var currentIndex = 0;
var element = document.getElementById("icon");  
var intervalId = window.setInterval(function(){
element.innerHTML=icons_list[currentIndex];
currentIndex = (currentIndex + 1) % length;
}, 5000);

    </script>
